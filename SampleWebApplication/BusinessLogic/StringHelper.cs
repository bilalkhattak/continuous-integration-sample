using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SampleWebApplication.BusinessLogic
{

    public class StringHelper
    {
        /// <summary>
        /// This function is written by Bilal Khattak
        /// This function returns total words count in a sentences
        /// </summary>
        /// <param name="sentence"></param>
        /// <param name="word"></param>
        /// <returns></returns>
        public static int GetWordOccurancesInASentence(string sentence, string word)
        {
            if (string.IsNullOrWhiteSpace(sentence))
            {
                return 0;
            }

            //replace any extra spaces with one space
            sentence = sentence.Replace("  ", " ");

            //splic sentence based on a space characture to get all the workds
            var allWords = sentence.Split(" ");

            int totalCount = 0;

            foreach (string w in allWords)
            {
                //remove
                string wrd = w.Replace(".", "");
                if (wrd.ToLowerInvariant() == word.ToLowerInvariant())
                {
                    //if word is founc increment the count by 1
                    totalCount += 1;
                }
            }

            return totalCount;
        }
        /// <summary>
        /// This function is written by Bilal Khattak
        /// This function validates an email address
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static bool ValidateEmailAddress(string emailAddress)
        {
            bool isValidEmailAddress = false;
            string Pattern = @"^[\w!#$%&'*+\-/=?\^_`{|}~]{1,64}(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,})|(\[([0-9]{1,3}\.){3}[0-9]{1,3}\]))$";
            Match emailAddressMatchRegx = Regex.Match(emailAddress, Pattern);
            isValidEmailAddress = emailAddressMatchRegx.Success;

            return isValidEmailAddress;
        }

        /// <summary>
        /// Author: Bilal Khattak
        /// This function returns a text limited by the word count
        /// </summary>
        /// <returns></returns>
        public static string ReturnLimitedText(string text, int wordCount)
        {
            if (string.IsNullOrEmpty(text))
            {
                return "";
            }

            var allWorkds = text.Split(" ");
            var limitedText = "";

            for (int i = 0; i < allWorkds.Length; i++)
            {
                if(i < wordCount)
                {
                    if(i == 0)
                    {
                        limitedText = allWorkds[i];
                    }
                    else
                    {
                        limitedText = limitedText + " " + allWorkds[i];
                    }
                }
            }

            return limitedText;
        }

        /// <summary>
        ///  Author: Bilal Khattak
        ///  Is valid swedish phone number
        /// </summary>
        /// <param name="number"></param>
        /// <returns></returns>
        public static bool IsPhoneNumberValid(string phonNumberString)
        {
            if (string.IsNullOrEmpty(phonNumberString))
            {
                return false;
            }
            string regularExpressionForPhone = @"^([\+]?46[-]?|[0])?[1-9][0-9]{8}$";
            var isValid = Regex.IsMatch(phonNumberString, regularExpressionForPhone);

            return isValid;

        }

        /// <summary>
        /// Author:  Muhammad shoaib
        /// </summary>
        /// <returns></returns>
        public static List<string> GetDistinctWordsUsedInASentence(string sentence)
        {
            sentence = sentence.Replace(".", "");
            sentence = sentence.Replace(",", "");
            sentence = sentence.Replace("  ", " ");
            sentence = sentence.ToLower();
            var words = sentence.Split(" ");

            var newList = new List<string>();
            for (int i = 0; i < words.Length; i++)
            {
                if (newList.Contains(words[i]) == false)
                {
                    newList.Add(words[i]);
                }
            }

            return newList;
        }

        /// <summary>
        /// Author: Bilal Khattak
        /// </summary>
        /// <param name="str1"></param>
        /// <param name="str2"></param>
        /// <returns></returns>
        public static string MixTwoStrings(string str1, string str2)
        {
            //Now implement the function
            var c1 = str1.ToCharArray();
            var c2 = str2.ToCharArray();
            //function is working now, lets test
            var maxLengthString = c1.Length;
            if (maxLengthString < c2.Length)
            {
                maxLengthString = c2.Length;
            }

            var finalString = "";
            for (int i = 0; i < c1.Length; i++)
            {
                if (i <= c1.Length)
                {
                    finalString = finalString + c1[i].ToString();
                }

                if (i <= c2.Length)
                {
                    finalString = finalString + c2[i].ToString();
                }
            }


            return finalString;
        }

    }
}
