using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SampleWebApplication.BusinessLogic
{
    public class VocabularyFlashCardHelper
    {
        public static Dictionary<string, string> Words = new Dictionary<string, string>();

        /// <summary>
        /// Added by Muhammad Omar Mansoor
        /// </summary>
        /// <param name="word"></param>
        public static string ReturnWordMeaning(string word)
        {
            if (string.IsNullOrEmpty(word))
            {
                return "";
            }

            var wordMeaning = "";
            foreach (var x in Words.Keys)
            {
                if(x == word)
                {
                    wordMeaning = Words[x];
                }
            }

            return wordMeaning;
        }

        /// <summary>
        /// Added by Muhammad Omar Mansoor
        /// </summary>
        /// <param name="word"></param>
        public static string ReturnWordWordForMeaning(string meaning)
        {
            if (string.IsNullOrEmpty(meaning))
            {
                return "";
            }

            var word = "";
            foreach (var x in Words)
            {
                if (x.Value == meaning)
                {
                    word = x.Key;
                }
            }

            return word;
        }

        /// <summary>
        /// Added by Muhammad Omar Mansoor
        /// </summary>
        /// <param name="word"></param>
        public static List<string> SearchWords(string somecharactersofword)
        {
            var listOfWords = new List<string>();
            if (string.IsNullOrEmpty(somecharactersofword))
            {
                return listOfWords;
            }

            foreach (var x in Words)
            {
                if (x.Key.Contains(somecharactersofword))
                {
                    listOfWords.Add(x.Key);
                }
            }

            return listOfWords;
        }

        /// <summary>
        /// Author: by Mahmoud Ehsani Ardakani
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        public static List<string> ReturnAllWordsWhereWordIsUsedInMeaning(string word)
        {
            var wordUsedInMeaning = new List<string>();
            if (string.IsNullOrEmpty(word))
            {
                return wordUsedInMeaning;
            }

            foreach (var x in Words)
            {
                if (x.Value.Contains(word))
                {
                    wordUsedInMeaning.Add(x.Key);
                }
            }

            return wordUsedInMeaning;
        }

    }
}
