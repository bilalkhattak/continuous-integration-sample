using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SampleWebApplication.BusinessLogic
{
    public class VocabularyBuilderHelper
    {
        /// <summary>
        /// Written by Mahmoud Ehsani Ardakani
        /// Return duplicate values in a list
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        public static List<string> FindDuplicateInList(List<string> lst)
        {
            var duplicationValues = new List<string>();

            for (int i = 0; i < lst.Count; i++)
            {
                var value = lst[i];

                for (int j = 0; j < lst.Count; j++)
                {
                    if (value == lst[j] && i != j)
                    {
                        if (duplicationValues.Contains(value) == false)
                        {
                            duplicationValues.Add(value);
                        }
                    }
                }
            }

            return duplicationValues;

        }

        /// <summary>
        /// Author: by Mahmoud Ehsani Ardakani
        /// clean list and remove duplicates
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        public static List<string> RemoveDuplicatesInList(List<string> lst)
        {
            var cleanList = new List<string>();

            for (int i = 0; i < lst.Count; i++)
            {
                var value = lst[i];

                if (cleanList.Contains(value) == false)
                {
                    cleanList.Add(value);
                }
            }

            return cleanList;
        }

        /// <summary>
        /// Author: Muhammad shoaib
        /// Removes html tags from a string
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string RemoveHtmlFromAString(string text)
        {
            var cleanString = Regex.Replace(text, "<.*?>", String.Empty);
            string removeScriptTags = "<(script|style)\\b[^>]*?>.*?</\\1>";
            cleanString = Regex.Replace(cleanString, removeScriptTags, "", RegexOptions.IgnoreCase | RegexOptions.Singleline);
            return cleanString;
        }

        /// <summary>
        /// Author:  Muhammad shoaib
        /// This function removes an item from list if the item exists
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        public static List<string> RemoveAnItemFromList(List<string> lst, string item)
        {
            var newList = new List<string>();
            for (int i = 0; i < lst.Count; i++)
            {
                if(item != lst[i])
                {
                    newList.Add(lst[i]);
                }
            }

            return newList;
        }

        /// <summary>
        /// Author:  Bilal Khattak
        /// </summary>
        /// <param name="lst"></param>
        /// <returns></returns>
        public static List<string> GetItemsWhereDoubleVowelsAreUsed(List<string> lst)
        {
            var lstDoubleVowel = new List<string>();
            lstDoubleVowel.Add("aa");
            lstDoubleVowel.Add("ae");
            lstDoubleVowel.Add("ai");
            lstDoubleVowel.Add("ao");
            lstDoubleVowel.Add("au");
            lstDoubleVowel.Add("ea");
            lstDoubleVowel.Add("ee");
            lstDoubleVowel.Add("ei");
            lstDoubleVowel.Add("eo");
            lstDoubleVowel.Add("eu");
            lstDoubleVowel.Add("ia");
            lstDoubleVowel.Add("ie");
            lstDoubleVowel.Add("ii");
            lstDoubleVowel.Add("io");
            lstDoubleVowel.Add("iu");
            lstDoubleVowel.Add("oa");
            lstDoubleVowel.Add("oe");
            lstDoubleVowel.Add("oi");
            lstDoubleVowel.Add("oo");
            lstDoubleVowel.Add("ou");
            lstDoubleVowel.Add("ua");
            lstDoubleVowel.Add("ue");
            lstDoubleVowel.Add("ui");
            lstDoubleVowel.Add("uo");
            lstDoubleVowel.Add("uu");

            var allWordDoubleVowel = new List<string>();
            for (int i = 0; i < lst.Count; i++)
            {
                for (int j = 0; j < lstDoubleVowel.Count; j++)
                {
                    if (lst[i].Contains(lstDoubleVowel[j]))
                    {
                        allWordDoubleVowel.Add(lst[i]);
                    }
                }
            }

            return allWordDoubleVowel;
        }

    }
}
