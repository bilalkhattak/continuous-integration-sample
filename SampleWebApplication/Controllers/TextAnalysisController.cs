﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SampleWebApplication.Controllers
{
    public class TextAnalysisController : Controller
    {
        // GET: TextAnalysisController
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Index(IFormCollection collection)
        {
            var text = collection["textToAnalyze"];
            var word = collection["word"];

            var count = BusinessLogic.StringHelper.GetWordOccurancesInASentence(text, word);

            ViewBag.text = text;
            ViewBag.word = word;
            ViewBag.count = count;

            return View();
        }

    }
}
