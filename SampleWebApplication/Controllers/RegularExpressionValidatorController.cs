using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SampleWebApplication.Controllers
{
    /// <summary>
    /// Author: Bilal Khattak
    /// </summary>
    public class RegularExpressionValidatorController : Controller
    {
        // GET: RegularExpressionValidatorController
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(IFormCollection collection)
        {
            var pattern = collection["regularExpression"];
            var text = collection["regularExpressionText"];


            // Create a Regex  
            var rg = new Regex(@"{\b[" + pattern + @"]\w+}");

            // Get all matches  
            var matchedWords = rg.Matches(text);

            var listMatchedWords = new List<string>();
            for (int count = 0; count < matchedWords.Count; count++)
            {
                listMatchedWords.Add(matchedWords[count].Value);
            }

            ViewBag.RegularExpression = pattern;
            ViewBag.RegularExpressionText = text;

            ViewBag.RegularExpressionresult = listMatchedWords;

            return View();
        }

    }
}
