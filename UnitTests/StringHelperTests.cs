using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    [TestClass]
    public class StringHelperTests
    {
        /// <summary>
        /// This unit test is written by Bilal Khattak
        /// It tests SampleWebApplication.BusinessLogic.StringHelper.ValidateEmailAddress function
        /// </summary>
        [TestMethod]
        public void TestValidateEmailAddress()
        {
            var isEmailAddressValid = SampleWebApplication.BusinessLogic.StringHelper.ValidateEmailAddress("test@gmail.com");
            Assert.IsTrue(isEmailAddressValid == true, "If condition is false then failed: StringHelper.ValidateEmailAddress not working properly");

            //invalid email
            isEmailAddressValid = SampleWebApplication.BusinessLogic.StringHelper.ValidateEmailAddress("test@");
            Assert.IsTrue(isEmailAddressValid == false, "If condition is false then failed: StringHelper.ValidateEmailAddress not working properly");
        }

        /// <summary>
        /// This unit test is written by Bilal Khattak
        /// It tests SampleWebApplication.BusinessLogic.StringHelper.ReturnLimitedText function
        /// </summary>
        [TestMethod]
        public void ReturnLimitedTextTest()
        {
            var text = SampleWebApplication.BusinessLogic.StringHelper.ReturnLimitedText("We like this assignment. It is really good. Furthermore, it is practical.", 4);
            Assert.IsTrue(text == "We like this assignment.", "If condition is false then failed: StringHelper.ReturnLimitedText not working properly");
        }

        /// <summary>
        /// This unit test is written by Bilal Khattak 
        /// It tests SampleWebApplication.BusinessLogic.StringHelper.GetWordOccurancesInASentence function
        /// </summary>
        [TestMethod]
        public void GetWordOccurancesTest()
        {
            var count = SampleWebApplication.BusinessLogic.StringHelper.GetWordOccurancesInASentence("This is test and again a test. For testing the test. Some more text.", "test");
            //there are total 3 test words in the sentence above
            Assert.IsTrue(count == 3, "StringHelper.GetWorkOccurancesInASentence does not work properly");

            count = SampleWebApplication.BusinessLogic.StringHelper.GetWordOccurancesInASentence("This is test and again a test. For testing the test.", "again");
            //there are total 1 again word in the sentence above
            Assert.IsTrue(count == 1, "StringHelper.GetWorkOccurancesInASentence does not work properly");
        }

        /// <summary>
        /// Author: Bilal Khattak 
        /// </summary>
        [TestMethod]
        public void TestIsPhoneNumberValid()
        {
            var result = SampleWebApplication.BusinessLogic.StringHelper.IsPhoneNumberValid("");
            Assert.IsTrue(result == false, "StringHelper.IsPhoneNumberValid failed");

            result = SampleWebApplication.BusinessLogic.StringHelper.IsPhoneNumberValid("223");
            Assert.IsTrue(result == false, "StringHelper.IsPhoneNumberValid failed");

            result = SampleWebApplication.BusinessLogic.StringHelper.IsPhoneNumberValid("+46764226525");
            Assert.IsTrue(result == true, "StringHelper.IsPhoneNumberValid failed");
        }

        /// <summary>
        /// Author: Muhammad shoaib
        /// </summary>
        [TestMethod]
        public void GetDistinctWordsUsedInASentenceTest()
        {
            var words = SampleWebApplication.BusinessLogic.StringHelper.GetDistinctWordsUsedInASentence("A lazy man is sleep in a bed. Also a lazy person is there.");
            //distinct words are: A lazy man is sleep in bed Also person there
            Assert.IsTrue(words.Count == 10, "GetDistinctWordsUsedInASentence failed");
        }

        /// <summary>
        /// Author: Bilal Khattak
        /// </summary>
        [TestMethod]
        public void MixTwoStringsTest()
        {
            //full test
            var mixWords = SampleWebApplication.BusinessLogic.StringHelper.MixTwoStrings("ABC", "123");
            //this should resuld into A1B2C3
            Assert.IsTrue(mixWords == "A1B2C3", "MixTwoStrings function failed");
            
        }



    }
}
