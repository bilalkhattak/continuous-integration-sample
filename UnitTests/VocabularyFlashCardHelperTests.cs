using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests
{
    /// <summary>
    /// Class added by bilal khattak
    /// </summary>
    [TestClass]
    public class VocabularyFlashCardHelperTests
    {
        /// <summary>
        /// Author: Muhammad Omar Mansoor
        /// </summary>
        [TestMethod]
        public void TestSearchWords()
        {
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Student", "a person who is studying at a university or other place of higher education");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Theatre", "outdoor area in which plays and other dramatic performances are given");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Thread", "a long, thin strand of cotton");
            
            var lst = SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.SearchWords("Th");            
            Assert.IsTrue(lst.Count == 2, "SearchWords failed");
        }

        /// <summary>
        /// Author: Muhammad Omar Mansoor
        /// </summary>
        [TestMethod]
        public void TestReturnWordWordForMeaning()
        {
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Orange", "a fruit");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("shceduler", "a time table");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("shceme", "plan");

            var word = SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.ReturnWordWordForMeaning("a time table");

            Assert.IsTrue(word == "shceduler", "ReturnWordWordForMeaning failed");

        }

        /// <summary>
        /// Author: Muhammad Omar Mansoor
        /// </summary>
        [TestMethod]
        public void TestReturnWordMeaning()
        {
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Best", "very good");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Player", "a person who plays something");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Table", "a furniture used for different puposes");

            var word = SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.ReturnWordMeaning("Best");

            Assert.IsTrue(word == "very good", "ReturnWordMeaning failed");

        }

        /// <summary>
        /// Author: Mahmoud Ehsani Ardakani
        /// </summary>
        [TestMethod]
        public void TestReturnAllWordsWhereWordIsUsedInMeaning()
        {
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("House", "a building where people live");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Office", "a building where people work");
            SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.Words.Add("Trainstation", "a place where train stops");

            var word = SampleWebApplication.BusinessLogic.VocabularyFlashCardHelper.ReturnAllWordsWhereWordIsUsedInMeaning("building");

            Assert.IsTrue(word.Count == 2, "ReturnAllWordsWhereWordIsUsedInMeaning failed");
        }

    }
}
