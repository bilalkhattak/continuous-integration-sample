using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace UnitTests
{
    [TestClass]
    public class VocabularyBuilderHelperTests
    {

        /// <summary>
        /// Author: Mahmoud Ehsani Ardakani
        ///This unit test for removing duplication value from a list.
        /// </summary>
        [TestMethod]
        public void TestRemoveDuplicatesInList()
        {
            var lst = new List<string>();
            lst.Add("item");
            lst.Add("dream");
            lst.Add("coding");
            lst.Add("item");
            lst.Add("coding");
            lst.Add("coding");
            lst.Add("New item");

            var cleanList = SampleWebApplication.BusinessLogic.VocabularyBuilderHelper.RemoveDuplicatesInList(lst);
            Assert.IsTrue(cleanList.Count == 4, "VocabularyBuilderHelper failed");
        
        }     
        
        /// <summary>
        /// Author: Mahmoud Ehsani Ardakani
        /// </summary>
        [TestMethod]
        public void TestFindDuplicateInList()
        {
            var lst = new List<string>();
            lst.Add("item");
            lst.Add("dream");
            lst.Add("coding");
            lst.Add("item");
            lst.Add("coding");
            lst.Add("New itm");

            var lstNew = SampleWebApplication.BusinessLogic.VocabularyBuilderHelper.FindDuplicateInList(lst);
            Assert.IsTrue(lstNew.Count == 2, "FindDuplicateInList failed");
        }

        /// <summary>
        /// Author: Muhammad Shoaib
        ///Test RemoveAnItemFromList
        /// </summary>
        [TestMethod]
        public void RemoveItemFromListTest()
        {
            var lst = new List<string>();
            lst.Add("A");
            lst.Add("B");
            lst.Add("C");
            lst.Add("D");

            var newList = SampleWebApplication.BusinessLogic.VocabularyBuilderHelper.RemoveAnItemFromList(lst, "B");
            Assert.IsTrue(newList.Count == 3, "RemoveAnItemFromList failed");
            Assert.IsTrue(newList.Contains("B") == false, "RemoveAnItemFromList failed");

        }

        /// <summary>
        /// Author: Muhammad Shoaib
        ///RemoveHtmlFromAString
        /// </summary>
        [TestMethod]
        public void TestRemoveHtmlFromAString()
        {
            var text = "This is <p>test</p> and some more text <br/> also some script <script>TTT</script>";
            var newText = SampleWebApplication.BusinessLogic.VocabularyBuilderHelper.RemoveHtmlFromAString(text);
            Assert.IsTrue(newText == "This is test and some more text  also some script TTT", "RemoveHtmlFromAString failed");
        }

        /// <summary>
        /// Author: Bilal Khattak
        /// </summary>
        [TestMethod]
        public void TestGetItemsWhereDoubleVowelsAreUsed()
        {
            var lst = new List<string>();
            lst.Add("Sometimes");
            lst.Add("people");
            lst.Add("sure");
            lst.Add("confuse");
            lst.Add("default");
            lst.Add("too");

            var wordsHavingDoubleVowels = SampleWebApplication.BusinessLogic.VocabularyBuilderHelper.GetItemsWhereDoubleVowelsAreUsed(lst);
            Assert.IsTrue(wordsHavingDoubleVowels.Count == 3, "function GetItemsWhereDoubleVowelsAreUsed not working properly");

            Assert.IsTrue(wordsHavingDoubleVowels.Contains("people") == true, "function GetItemsWhereDoubleVowelsAreUsed not working properly");
            Assert.IsTrue(wordsHavingDoubleVowels.Contains("default") == true, "function GetItemsWhereDoubleVowelsAreUsed not working properly");
            Assert.IsTrue(wordsHavingDoubleVowels.Contains("too") == true, "function GetItemsWhereDoubleVowelsAreUsed not working properly");
        }
    }
}
